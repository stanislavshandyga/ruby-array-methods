# frozen_string_literal: true

# Дан целочисленный массив и число К. Вывести количество элементов, меньших К.
def task_95(arr, k)
  arr.count { |v| v < k }
end

# Дан целочисленный массив. Вывести индексы элементов, которые больше своего
# правого соседа, и количество таких чисел.
def task_100(arr)
  {}.tap do |hsh|
    [].tap do |indexes|
      (0...arr.size - 1).each do |i|
        indexes << i if arr[i] > arr[i + 1]
      end

      hsh[:indexes] = indexes
      hsh[:amount] = indexes.size
    end
  end
end

# Дан целочисленный массив. Если данный массив образует убывающую
# последовательность, то вывести nil, в противном случае вывести номер первого
# числа, нарушающего закономерность.
def task_105(arr)
  arr.empty? ? return : value = arr[0]

  arr[1..-1].each_with_index do |v, i|
    v <= value ? value = v : (return i + 1)
  end

  nil
end

# Дан целочисленный массив. Поменять местами минимальный и максимальный
# элементы массива.
def task_110(arr)
  arr.size < 2 ? (return arr) : min = max = 0

  arr.each_with_index do |v, i|
    min = i if arr[min] > v
    max = i if arr[max] < v
  end

  arr[min], arr[max] = arr[max], arr[min]
end
