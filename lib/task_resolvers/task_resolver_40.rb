# frozen_string_literal: true

# Дан целочисленный массив. Найти количество максимальных элементов.
def task_40(arr)
  arr.count(arr.max)
end

# Дан целочисленный массив. Найти минимальный положительный элемент.
def task_45(arr)
  arr.select(&:positive?).min
end

# Дан целочисленный массив. Найти количество элементов, расположенных перед
# первым максимальным.
def task_50(arr)
  arr.empty? ? 0 : arr[0...arr.index(arr.max)].size
end

# Дан целочисленный массив. Найти количество элементов, расположенных после
# последнего максимального.
def task_55(arr)
  arr.empty? ? 0 : arr[arr.rindex(arr.max)...-1].size
end
