# frozen_string_literal: true

# Дан целочисленный массив. Найти количество элементов, между первым и
# последним максимальным.
def task_60(arr)
  if arr.empty?
    0
  else
    max = arr.max
    arr[(arr.index(max) + 1)...arr.rindex(max)].size
  end
end

# Дан целочисленный массив. Вывести вначале все его четные элементы, а затем -
# нечетные.
def task_65(arr)
  arr.each_with_index.inject([]) do |res, pair|
    pair[0].even? ? res.unshift(pair[0]) : res << pair[0]
  end
end

# Дано вещественное число R и массив вещественных чисел. Найти два элемента
# массива, сумма которых наименее близка к данному числу.
def task_70(v, arr)
  arr.size < 2 ? return : res = arr[0..1]

  arr.combination(2).each do |a, b|
    res = [a, b] if (v - res.sum).abs < (v - (a + b)).abs
  end

  res
end
