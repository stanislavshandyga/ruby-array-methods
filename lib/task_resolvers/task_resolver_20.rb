# frozen_string_literal: true

# Дан целочисленный массив. Найти минимальный из его локальных минимумов.
def task_20(arr)
  arr.size < 2 ? return : res = nil

  res = ValueChecker.check_first_value(arr, 0) || res
  res = ValueChecker.check_last_value(arr, arr.size - 1, res) || res

  (1...arr.size - 1).each do |i|
    res = ValueChecker.check_middle_value(arr, i, res) || res
  end

  res
end

class ValueChecker
  class << self
    def check_first_value(arr, i)
      arr[i] if arr[i] < arr[i + 1]
    end

    def check_last_value(arr, i, res)
      arr[i] if arr[i] < arr[i - 1] && (res.nil? || arr[i] < res)
    end

    def check_middle_value(arr, i, res)
      arr[i] if (arr[i] < arr[i - 1] && arr[i] < arr[i + 1]) &&
                (res.nil? || arr[i] < res)
    end
  end
end
