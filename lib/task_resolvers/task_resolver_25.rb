# frozen_string_literal: true

# Дан целочисленный массив. Преобразовать его, вставив перед каждым
# положительным элементом нулевой элемент.
def task_25(arr)
  i = 0

  while i < arr.size
    if arr[i].positive?
      arr.insert(i, arr[0])
      i += 1
    end

    i += 1
  end
end

# Дан целочисленный массив. Упорядочить его по убыванию.
def task_30(arr)
  arr
    .sort!
    .reverse!
end

# Дан целочисленный массив. Найти индекс первого минимального элемента.
def task_35(arr)
  arr.index(arr.min)
end
