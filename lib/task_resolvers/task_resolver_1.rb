# frozen_string_literal: true

# Дан целочисленный массив. Необходимо вывести вначале его элементы с четными
# индексами, а затем - с нечетными.
def task_1(arr)
  arr.each_with_index.inject([]) do |res, pair|
    pair[1].even? ? res.unshift(pair[0]) : res << pair[0]
  end
end

# Дан целочисленный массив. Преобразовать его, прибавив к четным числам первый
# элемент. Первый и последний элементы массива не изменять.
def task_5(arr)
  arr.size <= 2 ? return : first = arr.first

  (1...arr.size - 1).each do |i|
    arr[i] += first if arr[i].even?
  end
end

# Дан целочисленный массив. Заменить все положительные элементы на значение
# максимального.
def task_10(arr)
  max = arr.max

  arr.map! do |v|
    v.positive? ? max : v
  end
end

# Дан целочисленный массив. Проверить, образуют ли элементы арифметическую
# прогрессию. Если да, то вывести разность прогрессии, если нет - вывести nil.
def task_15(arr)
  raise 'Array size is less then 3.' if arr.size < 3

  arr[1..-2].each_with_index do |v, i|
    return nil if v != (arr[i] + arr[i + 2]) / 2
  end

  arr[-1] - arr[-2]
end
