# frozen_string_literal: true

# Список всех задач
# https://ru.wikibooks.org/wiki/Ruby/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%BD%D0%B8%D0%BA

require_relative 'task_resolvers/task_resolver_1'
require_relative 'task_resolvers/task_resolver_20'
require_relative 'task_resolvers/task_resolver_25'
require_relative 'task_resolvers/task_resolver_40'
require_relative 'task_resolvers/task_resolver_60'
require_relative 'task_resolvers/task_resolver_75'
require_relative 'task_resolvers/task_resolver_95'
