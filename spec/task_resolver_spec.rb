require 'task_resolver'

RSpec.describe 'task resolver' do
  it 'has needed methods' do
    expect(Object.private_method_defined?(:task_1)).to be_truthy
    expect(Object.private_method_defined?(:task_5)).to be_truthy
    expect(Object.private_method_defined?(:task_10)).to be_truthy
    expect(Object.private_method_defined?(:task_15)).to be_truthy
    expect(Object.private_method_defined?(:task_20)).to be_truthy
    expect(Object.private_method_defined?(:task_25)).to be_truthy
    expect(Object.private_method_defined?(:task_30)).to be_truthy
    expect(Object.private_method_defined?(:task_35)).to be_truthy
    expect(Object.private_method_defined?(:task_40)).to be_truthy
    expect(Object.private_method_defined?(:task_45)).to be_truthy
    expect(Object.private_method_defined?(:task_50)).to be_truthy
    expect(Object.private_method_defined?(:task_55)).to be_truthy
    expect(Object.private_method_defined?(:task_60)).to be_truthy
    expect(Object.private_method_defined?(:task_65)).to be_truthy
    expect(Object.private_method_defined?(:task_70)).to be_truthy
    expect(Object.private_method_defined?(:task_75)).to be_truthy
    expect(Object.private_method_defined?(:task_80)).to be_truthy
    expect(Object.private_method_defined?(:task_85)).to be_truthy
    expect(Object.private_method_defined?(:task_90)).to be_truthy
    expect(Object.private_method_defined?(:task_95)).to be_truthy
    expect(Object.private_method_defined?(:task_100)).to be_truthy
    expect(Object.private_method_defined?(:task_105)).to be_truthy
    expect(Object.private_method_defined?(:task_110)).to be_truthy
  end
end
