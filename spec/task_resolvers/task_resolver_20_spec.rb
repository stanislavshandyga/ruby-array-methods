RSpec.describe 'task resolver 20' do
  context 'task_20' do
    it 'works' do
      expect(task_20([1,2,2,2,2,2])).to eq(1)
      expect(task_20([2,2,2,2,2,1])).to eq(1)
      expect(task_20([2,2,8,7,8,9])).to eq(7)
      expect(task_20([2,1,8,7,8,9])).to eq(1)
      expect(task_20([2,3,8,7,8,9])).to eq(2)
      expect(task_20([2,3,8,7,8,1])).to eq(1)
      expect(task_20([1,2,3,4,5,6])).to eq(1)
      expect(task_20([])).to be_nil
      expect(task_20([1])).to be_nil
      expect(task_20([1,2])).to eq(1)
      expect(task_20([2,1])).to eq(1)
      expect(task_20([1,1])).to eq(nil)
      expect(task_20([1,1,1,1,1,1])).to eq(nil)
      expect(task_20([1,1,1,1,1,6])).to eq(nil)
    end
  end
end
