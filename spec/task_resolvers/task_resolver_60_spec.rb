RSpec.describe 'task resolver 60' do
  context 'task_60' do
    it 'works' do
      expect(task_60([])).to eq(0)
      expect(task_60([0])).to eq(0)
      expect(task_60([0,1])).to eq(0)
      expect(task_60([0,1,0])).to eq(0)
      expect(task_60([0,1,0,1])).to eq(1)
      expect(task_60([1,2,3,4,5,6])).to eq(0)
      expect(task_60([1,2,3,4,5,6,1,2,3,4,5])).to eq(0)
      expect(task_60([1,2,3,4,5,6,1,2,3,4,5,6])).to eq(5)
    end
  end

  context 'task_65' do
    it 'works' do
      expect(task_65([])).to eq([])
      expect(task_65([1])).to eq([1])
      expect(task_65([0,1])).to eq([0,1])
      expect(task_65([1,0])).to eq([0,1])
      expect(task_65([2,1,0])).to eq([0,2,1])
      expect(task_65([1,2,3,4,5,6])).to eq([6,4,2,1,3,5])
    end
  end

  context 'task_70' do
    it 'works' do
      expect(task_70(1, [])).to eq(nil)
      expect(task_70(1, [0])).to eq(nil)
      expect(task_70(1, [0,1])).to contain_exactly(0,1)
      expect(task_70(1, [0,1,1])).to contain_exactly(1,1)
      expect(task_70(1, [0,1,2])).to contain_exactly(1,2)
      expect(task_70(1, [0,1,2,3,4,5,6])).to contain_exactly(5,6)
      expect(task_70(5, [0,1,2,3,4,5,6])).to contain_exactly(5,6)
      expect(task_70(1.2, [0,1,2,3,4,5,6])).to contain_exactly(5,6)
      expect(task_70(11, [0,1,2,3,4,5,6,7])).to contain_exactly(0,1)
      expect(task_70(11, [7,6,5,4,3,2,1,0])).to contain_exactly(0,1)
      expect(task_70(-11, [0,1,2,3,4,5,6,7])).to contain_exactly(6,7)
      expect(task_70(-11, [-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7])).to contain_exactly(6,7)
      expect(task_70(0, [-10,-20,10,20])).to contain_exactly(-10,-20)
      expect(task_70(-0.1, [-10,-20,10,20])).to contain_exactly(10,20)
      expect(task_70(0, [10,20,-10,-20])).to contain_exactly(10,20)
      expect(task_70(0.1, [10,20,-10,-20])).to contain_exactly(-10,-20)
    end
  end
end
