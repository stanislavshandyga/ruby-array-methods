RSpec.describe 'task resolver 95' do
  context 'task_95' do
    it 'works' do
      expect(task_95([], 0)).to eq(0)
      expect(task_95([], 1)).to eq(0)
      expect(task_95([1], 0)).to eq(0)
      expect(task_95([0,1], 1)).to eq(1)
      expect(task_95([1,2], 3)).to eq(2)
      expect(task_95([1,2], 2)).to eq(1)
      expect(task_95([1,2,3,4,5,6,7], 4)).to eq(3)
    end
  end

  context 'task_100' do
    it 'works' do
      expect(task_100([])).to eq({indexes: [], amount: 0})
      expect(task_100([0])).to eq({indexes: [], amount: 0})
      expect(task_100([0,0])).to eq({indexes: [], amount: 0})
      expect(task_100([1,0])).to eq({indexes: [0], amount: 1})
      expect(task_100([0,1])).to eq({indexes: [], amount: 0})
      expect(task_100([0,1,1])).to eq({indexes: [], amount: 0})
      expect(task_100([0,1,2,3,4,5,6,7])).to eq({indexes: [], amount: 0})
      expect(task_100([7,6,5,4,3,2,1])).to eq({indexes: [0,1,2,3,4,5], amount: 6})
    end
  end

  context 'task_105' do
    it 'works' do
      expect(task_105([])).to be_nil
      expect(task_105([0])).to be_nil
      expect(task_105([0,0])).to be_nil
      expect(task_105([0,1])).to eq(1)
      expect(task_105([0,1,2,3])).to eq(1)
      expect(task_105([0,0,1,2,3])).to eq(2)
      expect(task_105([3,2,1,0])).to be_nil
      expect(task_105([3,2,2,1,0])).to be_nil
      expect(task_105([3,2,2,1,2])).to eq(4)
    end
  end

  context 'task_110' do
    it 'works' do
      arr = []
      task_110(arr)
      expect(arr).to eq([])

      arr = [0]
      task_110(arr)
      expect(arr).to eq([0])

      arr = [0,1]
      task_110(arr)
      expect(arr).to eq([1,0])

      arr = [1,0]
      task_110(arr)
      expect(arr).to eq([0,1])

      arr = [1,2,3,4,5,6,7]
      task_110(arr)
      expect(arr).to eq([7,2,3,4,5,6,1])
    end
  end
end
