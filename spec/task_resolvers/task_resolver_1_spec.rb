RSpec.describe 'task resolver 1' do
  context 'task_1' do
    it 'works' do
      expect(task_1([])).to eq([])
      expect(task_1([1])).to eq([1])
      expect(task_1([1,2])).to eq([1,2])
      expect(task_1([1,2,3])).to eq([3,1,2])
      expect(task_1([1,2,3,4,5,6])).to eq([5,3,1,2,4,6])
    end
  end

  context 'task_5' do
    it 'works' do
      arr = []
      task_5(arr)
      expect(arr).to eq([])

      arr = [1]
      task_5(arr)
      expect(arr).to eq([1])

      arr = [0,1]
      task_5(arr)
      expect(arr).to eq([0,1])

      arr = [1,2,3,4,5,6]
      task_5(arr)
      expect(arr).to eq([1,3,3,5,5,6])
    end
  end

  context 'task_10' do
    it 'works' do
      arr = [-1,2,3,4,5,6]
      task_10(arr)
      expect(arr).to eq([-1,6,6,6,6,6])
    end
  end

  context 'task_15' do
    it 'works' do
      expect { task_15([]) }.to raise_error('Array size is less then 3.')
      expect { task_15([1]) }.to raise_error('Array size is less then 3.')
      expect { task_15([1,2]) }.to raise_error('Array size is less then 3.')

      expect(task_15([1,2,3])).to eq(1)
      expect(task_15([1,2,3,4,5,6,7])).to eq(1)
      expect(task_15([2,5,8,11,14])).to eq(3)
      expect(task_15([1,3,3])).to be_nil
      expect(task_15([1,1,3])).to be_nil
    end
  end
end
