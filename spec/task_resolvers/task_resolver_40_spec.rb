RSpec.describe 'task resolver 40' do
  context 'task_40' do
    it 'works' do
      expect(task_40([])).to eq(0)
      expect(task_40([0])).to eq(1)
      expect(task_40([0,1])).to eq(1)
      expect(task_40([0,1,0,1,1])).to eq(3)
      expect(task_40([1,2,3,4,5,6])).to eq(1)
      expect(task_40([1,2,3,4,5,6,1,6])).to eq(2)
    end

    it 'works' do
      expect(task_45([])).to eq(nil)
      expect(task_45([0])).to eq(nil)
      expect(task_45([-1])).to eq(nil)
      expect(task_45([0,1])).to eq(1)
      expect(task_45([-1,1])).to eq(1)
      expect(task_45([-1,1,2])).to eq(1)
      expect(task_45([-1,2,3,4,5,6,-1,-6])).to eq(2)
    end

    it 'works' do
      expect(task_50([])).to eq(0)
      expect(task_50([0])).to eq(0)
      expect(task_50([0,1])).to eq(1)
      expect(task_50([-1,1,2])).to eq(2)
      expect(task_50([1,2,3,4,5,6])).to eq(5)
      expect(task_50([1,2,3,4,5,6,1,2,3,4,5,6])).to eq(5)
    end

    it 'works' do
      expect(task_55([])).to eq(0)
      expect(task_55([1])).to eq(0)
      expect(task_55([1,2])).to eq(0)
      expect(task_55([1,2,1])).to eq(1)
      expect(task_55([1,2,1,2])).to eq(0)
      expect(task_55([1,2,1,2,1,1])).to eq(2)
      expect(task_55([1,2,3,4,5,6,1,2,3,4,5,6])).to eq(0)
      expect(task_55([1,2,3,4,1,2,3,4,1])).to eq(1)
    end
  end
end
