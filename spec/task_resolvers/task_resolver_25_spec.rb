RSpec.describe 'task resolver 25' do
  context 'task_25' do
    it 'works' do
      arr = []
      task_25(arr)
      expect(arr).to eq([])

      arr = [1]
      task_25(arr)
      expect(arr).to eq([1,1])

      arr = [0]
      task_25(arr)
      expect(arr).to eq([0])

      arr = [-1]
      task_25(arr)
      expect(arr).to eq([-1])

      arr = [-1,1]
      task_25(arr)
      expect(arr).to eq([-1,-1,1])

      arr = [-1,1,2,3,4]
      task_25(arr)
      expect(arr).to eq([-1,-1,1,-1,2,-1,3,-1,4])
    end
  end

  context 'task_30' do
    it 'works' do
      arr = []
      task_30(arr)
      expect(arr).to eq([])

      arr = [1]
      task_30(arr)
      expect(arr).to eq([1])

      arr = [1,2]
      task_30(arr)
      expect(arr).to eq([2,1])

      arr = [1,2,3]
      task_30(arr)
      expect(arr).to eq([3,2,1])

      arr = [3,2,1]
      task_30(arr)
      expect(arr).to eq([3,2,1])
    end
  end

  context 'task_35' do
    it 'works' do
      expect(task_35([])).to eq(nil)
      expect(task_35([1])).to eq(0)
      expect(task_35([1,2])).to eq(0)
      expect(task_35([2,1])).to eq(1)
      expect(task_35([1,1,2])).to eq(0)
      expect(task_35([1,2,1])).to eq(0)
      expect(task_35([2,1,1])).to eq(1)
    end
  end
end
