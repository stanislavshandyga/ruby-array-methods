RSpec.describe 'task resolver 75' do
  context 'task_75' do
    it 'works' do
      expect(task_75([])).to eq(nil)
      expect(task_75([0])).to eq(0)
      expect(task_75([1])).to eq(1)
      expect(task_75([-1])).to eq(1)
      expect(task_75([-1,1])).to eq(1)
      expect(task_75([1,1])).to eq(1)
      expect(task_75([-1,-1])).to eq(1)
      expect(task_75([1,-1])).to eq(1)
      expect(task_75([1,2,3,4,5,-1,-2,-3,-4,-5])).to eq(3)
    end
  end

  context 'task_80' do
    it 'works' do
      expect(task_80(1,1)).to eq({arr: [], len: 0})
      expect(task_80(1,2)).to eq({arr: [], len: 0})
      expect(task_80(1,3)).to eq({arr: [2], len: 1})
      expect(task_80(-1,3)).to eq({arr: [0,1,2], len: 3})
      expect(task_80(3,-1)).to eq({arr: [0,1,2], len: 3})
    end
  end

  context 'task_85' do
    it 'works' do
      expect(task_85(0)).to eq(0)
      expect(task_85(1)).to eq(1)
      expect(task_85(2)).to eq(2)
      expect(task_85(3)).to eq(3)
      expect(task_85(4)).to eq(8)
      expect(task_85(5)).to eq(15)
      expect(task_85(6)).to eq(48)
    end
  end

  context 'task_90' do
    it 'works' do
      expect(task_90([0])).to eq(0)
      expect(task_90([1])).to eq(1)
      expect(task_90([2])).to eq(0)
      expect(task_90([3])).to eq(1)
      expect(task_90([4])).to eq(0)
      expect(task_90([0,1])).to eq(1)
      expect(task_90([0,1,2])).to eq(1)
      expect(task_90([0,1,2,3])).to eq(2)
      expect(task_90([0,1,2,3,4,5,6,7])).to eq(4)
    end
  end
end
